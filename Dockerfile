FROM node:12.6.0-alpine
EXPOSE 8080
COPY server.js .
CMD node server.js

#FROM golang:1.11

#WORKDIR /go/src/invoke

#COPY invoke.go .
#RUN go install -v

#COPY . .

#CMD ["invoke"]